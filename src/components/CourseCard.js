import {Container, Row, Col, Card, Button} from "react-bootstrap"
import {useState, useEffect} from "react"
import {Link} from "react-router-dom"

const CourseCard = ({courseProp}) => {

	const {courseName, description, isActive, price, _id} = courseProp

	const [isDisabled, setIsDisabled] = useState(false)

	return (
		<Container fluid>
			<Row className="m-3 justify-content-center">
				<Col xs={8} md={4}>
					<Card>
					  <Card.Body>
					  		<Card.Title>{courseName}</Card.Title>
					    	<Card.Subtitle>Course Description:</Card.Subtitle>
							<Card.Text>{description}</Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>{price}</Card.Text>
					    	<Link className="btn btn-primary" to={`/courses/${_id}`}>Details</Link>
					  </Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
	)
}

export default CourseCard