import {Container, Form, Button} from "react-bootstrap"
import Swal from "sweetalert2"
import {useState, useEffect} from "react"
import {useHistory} from "react-router-dom"

export default function Register() {

	const [email, setEmail] = useState("")
	const [password, setPassword] = useState("")
	const [cpw, setCpw] = useState("")
	const [firstName, setFirstName] = useState("")
	const [lastName, setLastName] = useState("")
	const [mobileNo, setMobileNo] = useState("")
	const [isDisabled, setIsDisabled] = useState(false)

	let history = useHistory()

	useEffect(() => {
		if(firstName !== '' && lastName !== '' && mobileNo !== '' && email !== '' && password !== '' && cpw !== '' && password === cpw && mobileNo.length === 11) {
			setIsDisabled(false)
		} else {
			setIsDisabled(true)
		}
	}, [firstName, lastName, mobileNo, email, password, cpw])

	const register = (event) => {
		event.preventDefault()
		fetch("http://localhost:4000/api/users/email-exists", {
			method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data === true) {
				//console.log('if')
				fetch("http://localhost:4000/api/users/register",{
					method: "POST",
					headers: {
						"Content-Type": "application/json"
					},
					body: JSON.stringify({
						firstname: firstName,
						lastname: lastName,
						mobileNo: mobileNo,
						email: email,
						password: password
					})
				})
				.then(res => res.json())
				.then(data => {
					if(data === true) {
						// console.log('registered')
						Swal.fire({
							title: "Registered succesfully!",
							icon: "success",
							text: "Login now!"
						})
						
					} else {
						// console.log('error')
						Swal.fire({
							title: "Something went wrong",
							icon: "error",
							text: "Please try again"
						})
					}
					history.push("/login")
				})
				
			} else {
				//console.log('else')
				Swal.fire({
					title: "Email exists!",
					icon: "error",
					text: "Try another email"
				})
			}
		})
		setEmail("")
		setPassword("")
		setCpw("")
		setFirstName("")
		setLastName("")
		setMobileNo("")

	}
	return (
		<Container>
			<Form 
				className="border p-3 m-4"
				onSubmit={(event) => register(event)}
			>
			  <Form.Group className="mb-3" controlId="firstName">
			    <Form.Label>First Name</Form.Label>
			    <Form.Control
		    		type="text"
		    		placeholder="Enter First Name"
		    		value={firstName}
		    		onChange={(event) => setFirstName(event.target.value)}
			    />
			    <Form.Text className="text-muted">
			    </Form.Text>
			  </Form.Group>
			  <Form.Group className="mb-3" controlId="lastName">
			    <Form.Label>Last Name</Form.Label>
			    <Form.Control
		    		type="text"
		    		placeholder="Enter Last Name"
		    		value={lastName}
		    		onChange={(event) => setLastName(event.target.value)}
			    />
			    <Form.Text className="text-muted">
			    </Form.Text>
			  </Form.Group>
			  <Form.Group className="mb-3" controlId="mobileNo">
			    <Form.Label>Mobile Number</Form.Label>
			    <Form.Control
		    		type="tel"
		    		placeholder="Enter Mobile Number"
		    		value={mobileNo}
		    		onChange={(event) => setMobileNo(event.target.value)}
			    />
			    <Form.Text className="text-muted">
			    </Form.Text>
			  </Form.Group>
			  <Form.Group className="mb-3" controlId="email">
			    <Form.Label>Email address</Form.Label>
			    <Form.Control
		    		type="email"
		    		placeholder="Enter email"
		    		value={email}
		    		onChange={(event) => setEmail(event.target.value)}
			    />
			    <Form.Text className="text-muted">
			    </Form.Text>
			  </Form.Group>
			  <Form.Group className="mb-3" controlId="password">
			    <Form.Label>Password</Form.Label>
			    <Form.Control
		    		type="password"
		    		placeholder="Password"
		    		value={password}
		    		onChange={(event) => setPassword(event.target.value)}
			    />
			  </Form.Group>
			  <Form.Group className="mb-3" controlId="cpw">
			    <Form.Label>Confirm Password</Form.Label>
			    <Form.Control
		    		type="password"
		    		placeholder="Verify Password"
		    		value={cpw}
		    		onChange={(event) => setCpw(event.target.value)}
			    />
			  </Form.Group>
			  {
			  	(isDisabled === false) ?
			  		<Button variant="success" type="submit" disabled={isDisabled}>
			  		  Submit
			  		</Button>
			  	:
			  		<Button variant="danger" type="submit" disabled={isDisabled}>
			  		  Submit
			  		</Button>
			  }
			</Form>
		</Container>
	)
}